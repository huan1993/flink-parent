# flink-parent

#### 介绍
flink学习,记录学习flink过程中的一些知识点  

#### 项目介绍
```
flink-parent
│ |- 父项目
├─flink-wordcount [博客](https://blog.csdn.net/fu_huo_1993/article/details/103107125)
│ |- 基于stream的word count 程序  
│ │     >> jdk需要8或更高,maven需要3.0.4或更高
│ │     >> mac 上使用 nc -l 9999 开启9999端口监听  
├─flink-lambda [博客](https://blog.csdn.net/fu_huo_1993/article/details/103108847)
│ |- 在flink中使用lambda表达式   
│ │     >> flink中使用lambda表达式，出错后的解决方案
│ │     >> 不建议使用lambda,推荐使用匿名类或别的
│ │     >> Types 可以指定使用lambda表达式后需要返回的类型      
```


#### 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request